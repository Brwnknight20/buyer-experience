// https://github.com/JustinBeckwith/linkinator/blob/main/src/index.ts
import fs from 'fs';
import { LinkChecker } from 'linkinator';

const checkForBrokenLinks = async () => {
  const fullSiteChecker = new LinkChecker();
  const absolutePath = 'http://about.gitlab.com/';

  // Scan all index.html files in the /dist directory for broken url's.
  // .check() is an AsyncFunction, let's wait for results.
  const results = await fullSiteChecker.check({
    path: '**/index.html',
    timeout: 5000,
    linksToSkip: [
      '_nuxt',
      'node_modules',
      'https://player.vimeo.com',
      'https://gitlab.com/users/sign_in',
      'https://www.linkedin.com',
      'https://ir.gitlab.com/',
      'https://support.gitlab.com/',
      'http://contributors.gitlab.com/',
      'https://gitlab.com/dagytran/diploma-thesis/blob/master/src/main.pdf',

      // Looks like there's bot detection on these
      'https://docs.deepfactor.io/hc/en-us/articles/1500008981941/',
      'https://blog.cloudflare.com/cloudflare-pages-partners-with-gitlab',
      'https://support.phrase.com/hc/en-us/articles/5709668128796/',
      'https://support.workboard.com/hc/en-us/articles/8045455704333-Key-Results-from-GitLab',
      'https://blogs.oracle.com/cloud-infrastructure/post/using-the-gitlab-cicd-pipelines-integrated-workflow-to-build-test-deploy-and-monitor-your-code-with-container-engine-for-kubernetes-and-registry',
      'https://www.make.com/en/integrations/gitlab',
      'https://cloud.google.com/functions',
    ],
  });

  const brokenLinks = results.links.filter((x) => x.state === 'BROKEN');
  const html = brokenLinks
    .map((brokenLink) => `<a href="${absolutePath}${brokenLink.url}"/>`)
    .join(' ');

  // inject all the broken links into a temporary html file.
  // We're going to re-run the scanner against the temporary file. Except this
  // time we're swapping the local path with http:://about.gitlab.com/. This
  // is because the local references to handbook, blog, etc. only work on prod.
  fs.writeFile('retry.html', html, () => console.log('Created retry.html'));
  const retryChecker = new LinkChecker();
  const retryResults = await retryChecker.check({
    path: 'retry.html',
    timeout: 5000,
  });

  console.log(`Scanned a total of ${results.links.length} links`);

  const broken = retryResults.links.filter((x) => x.state === 'BROKEN');
  console.log(`Found ${broken.length} broken links`);

  if (broken.length) {
    broken.forEach((brokenLink) =>
      console.log(
        `${brokenLink.status} - ${brokenLink.url.replace(absolutePath, '')}`,
      ),
    );
    process.exit(1);
  }
};

checkForBrokenLinks();
