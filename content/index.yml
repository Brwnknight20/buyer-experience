---
  title: The One DevOps Platform
  description: From planning to production, bring teams together in one application. Ship secure code more efficiently to deliver value faster.
  schema_org: >
    {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form our culture.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Our mission is to change all creative work from read-only to read-write so that everyone can contribute.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing - Challenger, DevOps Dozen award for the Best DevOps Solution Provider for 2019, 451 Firestarter Award from 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}
  # Saving homepage carousel content as it will be used in the near future
  # slides:
  #   - image: /nuxt-images/home/hero/background.png
  #     alt: background infinite loop image
  #     dark: false
  #     title: The One DevOps Platform
  #     subtitle: >-
  #       From planning to production, bring teams together in one application. Ship secure code more efficiently to deliver value faster. 
  #     btn_primary:
  #       text: Get free trial
  #       href: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
  #       ga_name: free trial
  #     btn_secondary:
  #       text: What is GitLab?
  #       href: ''
  #       icon: play
  #       modal: true
  #   - image: /nuxt-images/home/hero/background2.png
  #     alt: background infinite loop image
  #     dark: false
  #     title: Get started with GitLab
  #     subtitle: >-
  #       New to GitLab? Explore these resources to help you get the most out of
  #       GitLab and know what to expect along the way.
  #     btn_primary:
  #       text: Explore resources
  #       href: /get-started/
  #       ga_name: get started
  #     btn_secondary:
  #       text: Start your free trial
  #       href: /free-trial/
  #       ga_name: free trial
  #       icon: chevron-lg-right
  #   - image: /nuxt-images/home/hero/background3.png
  #     alt: background infinite loop image
  #     dark: true
  #     title: 2022 Global DevSecOps Survey
  #     subtitle: >-
  #       See what we learned from over 5,000 DevOps professionals about automation,
  #       security, AI, and more.
  #     btn_primary:
  #       text: Read the survey
  #       href: 'https://about.gitlab.com/developer-survey/'
  #       ga_name: devsecops survery
  #   - image: /nuxt-images/home/hero/background2.png
  #     alt: background infinite loop image
  #     dark: false
  #     title: See what you could save
  #     subtitle: >-
  #       Calculate the cost of your current toolchain and how much your organization
  #       could save by switching to The One DevOps Platform.
  #     btn_primary:
  #       text: Try the calculator
  #       href: /calculator/roi/
  #       ga_name: roi calculator
  cta_block:
    title: Software. Faster.
    subtitle: From planning to production, GitLab brings teams together to shorten cycle times, reduce costs, strengthen security, and increase developer productivity.
    # Remove comments and add content to add pill badge to Homepage hero
    # badge:
    #   text: "Connect with new people and ideas at GitLab Commit 2022 →"
    #   link: "/events/commit/"
    #   data_ga_name: "commit pill"
    #   data_ga_location: "hero"
    primary_button:
      text: "Get free trial"
      link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
      data_ga_name: "free trial"
      data_ga_location: "hero"
    secondary_button:
      text: "What is GitLab?"
      modal:
        video_link: https://player.vimeo.com/video/702922416?h=06212a6d7c
      data_ga_name: "watch demo"
      data_ga_location: "hero"
    image_copy: DevSecOps platform
    images:
      - id: 1
        image: "/nuxt-images/home/Manage.png"
        alt: "Brand image of GitLab boards"
      - id: 2
        image: "/nuxt-images/home/Verify.png"
        alt: "Brand image of GitLab product"
      - id: 3
        image: "/nuxt-images/home/Plan.png"
        alt: "Brand image of GitLab roadmap"
  customer_logos_block:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link to Siemens customer case study
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/Nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: Link to blogpost How UBS created their own DevOps platform using GitLab
        alt: "UBS logo"
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
  featured_content:
    cards:
      - header: Get started with GitLab
        description: New to GitLab? Explore these resources to help you get the most out of GitLab and know what to expect along the way. 
        link: 
          href: /get-started/
          text: Explore resources
          data_ga_name: get started
          data_ga_location: home content block
        icon: cog-check
      - header: 2022 Global DevSecOps Survey
        description: See what we learned from over 5,000 DevOps professionals about automation, security, AI, and more.
        link:
          href: /developer-survey/
          text: Read the survey
          data_ga_name: dev survey
          data_ga_location: home content block
        icon: doc-pencil-alt
      - header: See what you could save 
        description: Calculate the cost of your current toolchain and how much your organization could save by switching to The One DevOps Platform.
        link: 
          href: /calculator/roi/
          text: Try the calculator
          data_ga_name: roi calculator
          data_ga_location: home content block
        icon: money
  resource_card_block:
    column_size: 4
    header_cta_text: View all resources
    header_cta_href: /resources/
    header_cta_ga_name: view all resources
    header_cta_ga_location: body
    cards:
      - icon:
          name: ebook-alt
          variant: marketing
          alt: Ebook Icon
        event_type: "Ebook"
        header: "Beginner's Guide to DevOps"
        link_text: "Read more"
        href: "https://page.gitlab.com/resources-ebook-beginners-guide-devops.html"
        image: "/nuxt-images/home/resources/Devops.png"
        alt: Winding path
        data_ga_name: "Beginner's Guide to DevOps"
        data_ga_location: "body"
      - icon:
          name: topics
          variant: marketing
          alt: Topics Icon
        event_type: "Topics"
        header: "What is CI/CD?"
        link_text: "Read more"
        href: "/topics/ci-cd/"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
        alt: Text reading CI/CD on a gradient
        data_ga_name: "Collaboration without Boundaries"
        data_ga_location: "body"
      - icon:
          name: report-alt
          variant: marketing
          alt: Report Icon
        event_type: "Report"
        header: "2022 Global DevSecOps Survey"
        link_text: "Read more"
        href: "/developer-survey/"
        image: "/nuxt-images/home/DevSecOps-thumbnail.jpeg"
        alt: Checkmark in sidewalk chalk
        data_ga_name: "2022 Global DevSecOps Survey"
        data_ga_location: "body"
      - icon:
          name: blog-alt
          variant: marketing
          alt: Blog Icon
        event_type: "Blog post"
        header: "Comply with NIST's secure software supply chain framework with GitLab"
        link_text: "Read more"
        href: "/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/"
        image: "/nuxt-images/home/resources/NIST.png"
        alt: City at night
        data_ga_name: "Comply with NIST's secure software supply chain framework with GitLab"
        data_ga_location: "body"
      - icon:
          name: partners
          variant: marketing
          alt: Partners Icon
        event_type: "Partners"
        header: "Discover the benefits of GitLab on AWS"
        link_text: "Read more"
        href: "/partners/technology-partners/aws/"
        image: "/nuxt-images/home/resources/AWS.png"
        alt: "Text reading AWS on a gradient"
        data_ga_name: "Discover the benefits of GitLab on AWS"
        data_ga_location: "body"
      - icon:
          name: announce-release
          variant: marketing
          alt: Announce Release Icon
          hex_color: '#FFF'
        event_type: "Release"
        header: "GitLab 15.5 released with GitLab Cloud Seed and Autocomplete suggestions"
        link_text: "Read more"
        href: "/releases/2022/10/22/gitlab-15-5-released/"
        alt: "GitLab version number on a gradient"
        image: "/nuxt-images/home/resources/15_5.png"
        data_ga_name: "GitLab 15.5 released with GitLab Cloud Seed and Autocomplete suggestions"
        data_ga_location: "body"

  quotes_carousel_block:
    header: Teams do more with GitLab
    header_link:
      url: /customers/
      text: Read our case studies
      data_ga_name: Read our case studies
      data_ga_location: body
    quotes:
      - main_img:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: UBS logo
        quote: "\u0022We have an expression at UBS, ‘all developers wait at the same speed,’ so anything we can do to reduce their waiting time is value added. And GitLab allows us to have that integrated experience. Our developers should be able to build cloud-native applications on a cloud-native platform and have the best-in-class developer experience. That was our goal.\u0022"
        author: Rick Carey Group Chief Technology Officer
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        data_ga_name: ubs
        data_ga_location: home case studies
      - main_img:
          url: /nuxt-images/home/logo_chorus_color.svg
          alt: Chorus.ai logo
        quote: "\u0022During a recent audit for SOC2 compliance, the auditors said that Chorus had the fastest auditing process they have seen and most of that is due to the capabilities of GitLab.\u0022"
        author: Russell Levy, CTO of Chorus.ai
        url: /customers/chorus/
        data_ga_name: chorus
        data_ga_location: home case studies
      - main_img:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: Iron Mountain logo
        quote: "\u0022What's great about GitLab is the single pane of glass it provides for scaling an Agile model.\u0022"
        author: Josh Langley, VP of Enterprise Architecture and Platforms, Iron Mountain
      - main_img:
          url: /nuxt-images/home/logo_siemens_mono.svg
          alt: Siemens logo
        quote: "\u0022Our customers and developers want to have a reliable service [like Gitlab] that is running all the time.\u0022"
        author: Roger Meier, Principal Key Expert and Service Owner of code.siemens.com from Siemens IT
        url: /customers/siemens/
        data_ga_name: siemens
        data_ga_location: home case studies

  solutions_block:
    title: One platform, one team
    image: /nuxt-images/home/solutions/solutions-top-down.png
    alt: "Top down image of office"
    description: "Whether you're starting by integrating a few point solutions, or simplifying your entire toolchain, now you can do it as one team in one platform. Collaborating from planning to production across one platform, with security built-in."
    subtitle: The way DevOps should be
    sub_image: /nuxt-images/home/solutions/solutions.png
    solutions:
      - title: Accelerate your digital transformation
        description: Reach your digital transformation objectives faster with a DevOps platform for your entire organization.
        icon:
          name: accelerate
          alt: Accelerate Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/digital-transformation/
        data_ga_name: digital transformation
        data_ga_location: body
        image: /nuxt-images/home/solutions/accelerate.png
        alt: "Text bubbles of communicating teams"
      - title: Deliver software faster
        description: Automate your software delivery process so you can deliver value faster and quality code more often.
        icon:
          name: deliver
          alt: Deliver Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/delivery-automation/
        data_ga_name: delivery automation
        data_ga_location: body
        image: /nuxt-images/home/solutions/deliver.png
        alt: "Text bubbles of communicating teams"
      - title: Ensure compliance
        description: Simplify continuous software compliance by defining, enforcing and reporting on compliance in one platform.
        icon:
          name: ensure
          alt: Ensure Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: continuous software compliance
        data_ga_location: body
        image: /nuxt-images/home/solutions/ensure.png
        alt: "Text bubbles of communicating teams"
      - title: Build in security
        description: Adopt DevSecOps practices with continuous software security assurance across every stage.
        icon:
          name: shield-check-light
          alt: Shield Check Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/continuous-software-security-assurance/
        data_ga_name: continuous software security assurance
        data_ga_location: body
        image: /nuxt-images/home/solutions/build.png
        alt: "Text bubbles of communicating teams"
      - title: Improve collaboration and visibility
        description: Give everyone one platform to collaborate and see everything from planning to production.
        icon:
          name: improve
          alt: Improve Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/devops-platform/
        data_ga_name: devops platform
        data_ga_location: body
        image: /nuxt-images/home/solutions/improve.png
        alt: "Text bubbles of communicating teams"
  badges: 
    header: GitLab is The DevSecOps Platform 
    tabs :
      - tab_name: Leaders in DevOps
        tab_id: leaders
        tab_icon:  /nuxt-images/icons/ribbon-check-transparent.svg
        copy: |
          **Our users have spoken.**
          GitLab ranks as a G2 Leader across DevOps categories
        cta:
          url: /analysts
          data_ga_name: analysts page
          data_ga_location: leaders in devops tab - badge section
        badge_images:
          - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
            alt: G2 Enterprise Leader - Fall 2022
          - src: /nuxt-images/badges/midmarketleader_fall2022.svg
            alt: G2 Mid-Market Leader - Fall 2022
          - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
            alt: G2 Small Business Leader - Fall 2022
          - src: /nuxt-images/badges/bestresults_fall2022.svg
            alt: G2 Best Results - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
            alt: G2 Best Relationship Enterprise - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
            alt: G2 Best Relationship Mid-Market - Fall 2022
          - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
            alt: G2 Easiest To Do Business With Mid-Market - Fall 2022
          - src: /nuxt-images/badges/bestusability_fall2022.svg
            alt: G2 Best Usability - Fall 2022 
      - tab_name: Industry Analyst Research
        tab_id: research
        tab_icon: /nuxt-images/icons/doc-pencil-transparent.svg
        copy: |
          **What Industry Analysts are saying about GitLab**
        cta:
          url: /analysts
          data_ga_name: analysts page
          data_ga_location: industry analyst research tab - badge section
        analysts:
          - logo: /nuxt-images/logos/forrester-logo.svg
            text: 'The 2019 Forrester Wave™: Cloud-Native Continuous Integration Tools'
            link: 
              url: https://about.gitlab.com/analysts/forrester-cloudci19/
              data_ga_name: Forrester Cloud-Native Continuous Integration Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant for Enterprise Agile Planning Tools'
            link: 
              url: https://about.gitlab.com/analysts/gartner-eapt21/
              data_ga_name: Gartner Magic Quadrant for Enterprise Agile Planning Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant™ for Application Security Testing'
            link: 
              url: https://about.gitlab.com/analysts/gartner-ast22/
              data_ga_name: Gartner Magic Quadrant for Application Security Testing
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2021 Gartner® Market Guide for Value Stream Delivery Platforms'
            link: 
              url: https://about.gitlab.com/analysts/gartner-vsdp21/
              data_ga_name: Gartner Market Guide for Value Stream Delivery Platforms
  top-banner: 
    text: The 2022 DevSecOps Survey is here, with insights from 5,000 DevOps pros.
    link:
      text: Explore the survey
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right

