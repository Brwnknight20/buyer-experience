---
  title: 'TeamOps: Optimizing Team Efficiency | GitLab'
  og_itle: 'TeamOps: Optimizing Team Efficiency | GitLab'
  description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  twitter_description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  og_description: TeamOps is a results-focused team management discipline that reduces decision-making blockers to ensure fast and efficient strategic executions. Learn more!
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
        show: true
    title: |
      Better teams.
      Faster progress.
      Better world.
    subtitle: Making teamwork an objective discipline
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: team ops hero image
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Enroll your team
      data_ga_name: enroll your team
      data_ga_location: body
  spotlight:
    title: |
      TeamOps drives decision velocity
    subtitle: TeamOps is an operations model that helps teams maximize productivity, flexibility, and autonomy by managing decisions, information, and tasks more efficiently.
    description:
      "Creating the environment for better decisions and improved execution of them makes for better teams — and ultimately, progress.\n\n\n
      TeamOps is how GitLab scaled from a startup to a global public company in a decade. Now we're opening it up to every organization."
    list:
      title: Common pain points
      items:
        - Ad hoc workflows prevent alignment
        - DIY Management breeds dysfunction
        - Communication infrastructure is an afterthought
        - Obsession over consensus thwarts innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Make your team better
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
      title: |
        Built for all teams.
        Remote, Hybrid, or Office.
      image:
        url: /nuxt-images/team-ops/reasons-to-believe.png
        alt: Reasons to Believe in Team Ops image
      accordion:
        is_accordion: false
        items:
            - icon:
                name: group
                alt: User Group Icon
                variant: marketing
              header: A New Objective, Results-Focused Management Discipline
              text: TeamOps helps organizations make greater progress, by treating how your team members relate as a problem that can be operationalized.
            - icon:
                name: clipboard-check-alt
                alt: Clipboard Checkmar Icon
                variant: marketing
              header: A Field-Tested System
              text: We’ve been building and using TeamOps at GitLab for the past 5 years. As a result, our organization is more productive and our team members evidence greater job satisfaction. It was created here, but we believe it can help almost any organization.
            - icon:
                name: principles
                alt: Continuous Integration Icon
                variant: marketing
              header: Guiding Principles
              text: TeamOps is grounded in four Guiding Principles that can help organizations cooly, rationally navigate the dynamic, changing nature of work.
            - icon:
                name: cog-user-alt
                alt: Cog User Icon
                variant: marketing
              header: Action Tenets
              text: Each Principle is supported by a set of Action Tenets – behavior-based ways of working that can be implemented immediately.
            - icon:
                name: case-study-alt
                alt: Case Study Icon
                variant: marketing
              header: Real-World Examples
              text: We bring the Action Tenets to life with a growing library of real output based examples of this Tenet as practiced at Gitlab.
            - icon:
                name: verification
                alt: Ribbon Check Icon
                variant: marketing
              header: TeamOps Course
              text: We create alignment through the TeamOps Course, enabling teams and companies to experience the framework in a shared environment.
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 200
      accordion_aos_animation: fade-left
      accordion_aos_duration: 1600
      aaccordion_os_offset: 200
  video_spotlight:
    title: |
      Get immersed in TeamOps
    subtitle: The world has plenty of opinions on the future of work.
    description:
      "The TeamOps Course helps organizations make greater progress, by treating how your team members relate as a problem that can be operationalized.\n\n\n
      We’ve been building and using TeamOps at GitLab for the past 5 years. As a result our organization is more productive and our team members evidence greater job satisfaction. It was created here, but we believe it can help almost any organization."
    video:
      url: 754916142?h=56dd8a7d5d
      alt: team ops hero image
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Enroll now
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      Guiding Principles of TeamOps
    subtitle: Organizations need people and teams—their creativity, perspectives, and humanity.
    cards:
      - title: Shared reality
        description: |
          While other management philosophies prioritize the speed of knowledge transfer, TeamOps optimizes for the speed of knowledge retrieval.
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: Learn More
          url: /handbook/teamops/shared-reality/
        color: '#FCA326'
      - title: Everyone contributes
        description: |
          Organizations must create a system where everyone can consume information and contribute, regardless of level, function, or location.
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: Learn More
          url: /handbook/teamops/everyone-contributes/
        color: '#FC6D26'
      - title: Decision velocity
        description: |
          Success is correlated with decision velocity: the quantity of decisions made in a particular stretch of time and the results that stem from faster progress.
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: Learn More
          url: /handbook/teamops/decision-velocity/
        color: '#6D33CC'
      - title: Measurement clarity
        description: |
          This is about measuring the right things. TeamOps’ decision-making principles are only useful if you execute and measure results.
        icon:
          name: target
          slp_color: surface-700
        link:
          text: Learn More
          url: /handbook/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      Join the movement
    description:
      "TeamOps is an operations model that helps teams maximize productivity, flexibility, and autonomy by managing decisions, information, and tasks more efficiently. \n\n\n
      Join a growing list of organizations who are practicing TeamOps."
    list:
      title: Common pain points
      items:
        - Ad hoc workflows prevent alignment
        - DIY Management breeds dysfunction
        - Communication infrastructure is an afterthought
        - Obsession over consensus thwarts innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Enroll now
      data_ga_name: make your team better
      data_ga_location: body
    quotes:
      - text: Pre-deployment tests have provided more confidence that the product is ready to be released; also delivery frequency has increased.
        author: John Lastname
        note: Director of Job Title, Company name
      - text: Pre-deployment tests have provided more confidence that the product is ready to be released; also delivery frequency has increased.
        author: John Lastname
        note: Director of Job Title, Company name
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemens logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4 logo
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
