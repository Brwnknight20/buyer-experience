---
  title: GitLab vs. Digital.ai
  hero:
    title: GitLab vs. Digital.ai
    subtitle: How does GitLab compare to Digital.ai in the Manage stage?
    icon:
      name: manage-alt-2
      alt: Manage Icon
      variant: marketing
      size: md
    crumbs:
      - title: DevOps maturity comparison
        href: /competition/
        data_ga_name: Competition
        data_ga_location: breadcrumb
      - title: GitLab vs. Digital.al
  overview:
    comparisons:
      - title: GitLab
        image: /nuxt-images/developer-tools/harvey-balls/75.svg
      - title: Digital.ai
        image: /nuxt-images/developer-tools/harvey-balls/25.svg
    left_card:
      title: Digital.ai’s acquisitions have allowed it to become a top value stream platform.
      description: |
        Digital.ai was formed through a combination of several separate companies in an effort to create a “intelligent Value Stream Platform” to enable enterprise DevOps. Digital.ai excels in some features of our defined Manage stage with the capabilities of specific products from the suite.

        GitLab differentiates from Digital.ai with its ability to organize via Subgroups to help organizations align best with their Product Suite Value Streams. GitLab’s ability to measure DevOps adoption and see the impact on velocity is a standalone and unique feature, though it is still nascent in many categories. Digital.ai’s Team Forge product excels above GitLab in its ability to address Compliance Management, Permissions and User Management. However, GitLab is closing the gap with product capability improvements. The [Manage Stage Product Roadmap](https://about.gitlab.com/direction/manage/#vision-themes) fills these gaps with a focus on _All-in on SaaS_, _GitLab the business hub_, _fully managed compliance_, and _shortening time-to-value_.
    right_card:
      title: GitLab's product roadmap
      bullets:
        - title: A focus on the [workspace concept](https://gitlab.com/groups/gitlab-org/-/epics/4257) in the next few milestones.
        - title: New [customizable roles and permissions feature](https://gitlab.com/groups/gitlab-org/-/epics/4035).
        - title: Enhanced and improved [Value Stream Analytics (VSA) usability](https://gitlab.com/groups/gitlab-org/-/epics/7961) for deeper actionable insights into the entire work process performance.
      button:
        text: GitLab releases
        link: /releases/
  analysis:
    side_by_side:
      stage: Manage
      icon:
        name: manage-alt-2
        alt: Manage Icon
        variant: marketing
        size: md
      tabs:
        - title: Subgroups
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Projects can be organized and access to controlled resources can be restricted.
              sections:
                - title: Details
                  content: |
                    * GitLab groups can be organized into subgroups. Subgroups can be used to:
                      * Separate internal and external organizations. Because every subgroup can have its own visibility level, you can host groups for different purposes under the same parent group.
                      * Organize large projects. You can use subgroups to give different access to parts of the source code.
                      * Manage people and control visibility. Give a user a different role for each group they're a member of.
                    * Subgroups can:
                      * Belong to one immediate parent group.
                      * Have many subgroups.
                      * Be nested up to 20 levels.
                      * Use runners registered to parent groups.
                      * Secrets configured for the parent group are available to subgroup jobs.
                      * Users with the Maintainer role in projects that belong to subgroups can see the details of runners registered to parent groups.
                - title: Improving our product capabilities
                  content: |
                    * The Workspace group will be focussing on the workspace concept in the next few milestones. This will be the top-level namespace for you to manage everything GitLab. We will start by introducing issues into this top level group via [gitlab&4257](https://gitlab.com/groups/gitlab-org/-/epics/4257).
                    * We will also continue to make targeted investments in the short-term to advance our SaaS experience.
                    * Additional control:
                      * Administrators for GitLab.com groups need additional user management capabilities to keep their organization's data secure. Group administrators need to be able to manage all attributes and the lifecycle of accounts just like administrators on a self-managed instance can. We've introduced the concept of an Enterprise Account, which is used by an individual but is owned by an Enterprise. In our new subscription agreement. We'll support this in the system by:
                        * Continuing to build out functionality to give additional management capabilities for group administrators. We'll continue to consider accounts that were provisioned in the context of a group as Enterprise Accounts.
                        * In line with our subscription agreement changes, we will allow group administrators to claim a domain. We will build group level user management capabilities for users whose emails match a group's claimed domain.
                    * Flexible hierarchies
                    * Self-managed feature parity
                      * Management of features and settings across projects and groups is a challenge for GitLab administrators. We will be investing in workspaces to provide administrators with tools to effectively manage collections of groups.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/group/subgroups/
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Not offered by Digital.ai.
        - title: Permissions
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Users have different abilities depending on the role they have in a particular group or project. If a user is both in a project’s group and the project itself, the highest role is used.
              sections:
                - title: Details
                  content: |
                    * Instance-wide user permissions: By default, users can create top-level groups and change their usernames. A GitLab administrator can configure the GitLab instance to modify this behavior.
                    * Project members permissions: A user’s role determines what permissions they have on a project.
                - title: Improving our product capabilities
                  content: |
                    * We have written our first code against the new [customizable roles and permissions](https://gitlab.com/groups/gitlab-org/-/epics/4035) feature. It's a large effort, but it's really exciting to have this under way after a lot of customer discovery work. On the UX side, we are currently doing usability interviews with customers, to make sure the UI we have drafted is intuitive.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/permissions.html
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: TeamForge enables unique granular, pervasive RBAC across the enterprise architecture and automatically replicates credentials for remote repositories.
              sections:
                - title: Details
                  content: |
                    * Protect IP from rogue or accidental tampering and prevent unauthorized code changes with fine grained hierarchical role-based access controls (RBAC) and permissions management that span TeamForge and many other integrated tools, including Jenkins and Nexus. Define and classify role types for internal and external teams and securely delegate project tasks. Customize granular user permissions, while further centralizing role management using roles and project groups. Assign roles automatically following a common security model as you add new projects.
              additional:
                title: Additional
                content: |
                  * This is the Digital.ai TeamForge product as part of the Digital.ai suite.
              button:
                title: Documentation
                link: https://docs.digital.ai/bundle/teamforge220/page/index.html
        - title: User Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: User Management covers the lifecycle of a user within GitLab - how they are provisioned, de-provisioned, and the steps in between.
              sections:
                - title: Details
                  content: |
                    * We offer banning and blocking capabilities for users. A lot of our Enterprise Users features will allow extra capabilities for user management. User Profile, and Group and Project membership are owned by the Workspace team.
                - title: Improving our product capabilities
                  content: |
                    * Here is a list of [open issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AUser%20Management&first_page_size=100) that are in the User Management category.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/profile/account/create_accounts.html
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: TeamForge enables unique granular, pervasive RBAC across the enterprise architecture and automatically replicates credentials for remote repositories.
              sections:
                - title: Details
                  content: |
                    * Protect IP from rogue or accidental tampering and prevent unauthorized code changes with fine grained hierarchical role-based access controls (RBAC) and permissions management that span TeamForge and many other integrated tools, including Jenkins and Nexus. Define and classify role types for internal and external teams and securely delegate project tasks. Customize granular user permissions, while further centralizing role management using roles and project groups. Assign roles automatically following a common security model as you add new projects.
              additional:
                title: Additional
                content: |
                  * This is a repeat of Permissions where the two are complimentary
                  * This is the Digital.ai TeamForge product as part of the Digital.ai suite
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/profile/account/create_accounts.html
        - title: Credential Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Lifecycle management of credentials, including tokens, for both users and group owners/admins.
              sections:
                - title: Details
                  content: |
                    * Credential Inventory for GitLab.com (e.g. SSH and PAT expiration) provides administrators with visibility and control for the credentials that can access their instance or group(s). Today much of this functionality is only available to self-managed instances.
                - title: Improving our product capabilities
                  content: |
                    * What does success look like? GitLab provides a built-in experience that can be used to monitor, revoke, and ensure rotation of the credentials used to access GitLab. We currently have the Credential Inventory to provide some of this information on self-managed GitLab and will continue to enhance it to provide more information and to work for customers on SaaS.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/admin_area/credentials_inventory.html
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Mobile authentication establishes user authentication for the enterprise app store and for the mobile apps themselves without maintaining an entirely separate set of user credentials.
              sections:
                - title: Details
                  content: |
                    * Digital.ai App Management solves this problem with a system that uses the SAML 2.0 and Oauth standards to create an SSO experience for users by connecting mobile authentication to the enterprise’s chosen authentication identity provider (e.g. Active Directory).
                    * Biometric authentication is also supported, so that an organization’s enterprise app users can use TouchID and FaceID as they are accustomed to with their consumer apps.
              additional:
                title: Additional
                content: |
                  * This is the Digital.ai Mobile Application Management product as part of the Digital.ai suite
        - title: DevOps Reports
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description:  Get an overview of how well your organization is adopting DevOps and to see the impact on your velocity.
              sections:
                - title: Details
                  content: |
                    * The DevOps Score tab displays usage of major GitLab features on your instance over the last 30 days, averaged over the number of billable users in that time period. Users can also see the Leader usage score, calculated from top-performing instances based on Service Ping data that GitLab has collected. The score is compared to the lead score of each feature and then expressed as a percentage at the bottom of said feature. The overall DevOps Score is an average of feature scores.
                    * Service Ping data is aggregated on GitLab servers for analysis. Usage information is not sent to any other GitLab instances. It might take a few weeks for data to be collected before this feature is available for new GitLab users.
                - title: Improving our product capabilities
                  content: |
                    * Group Optimize is currently focused on usability improvements, aligned with the company goal to improve the System Usability Scale (SUS) score.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_reports.html
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Not offered by Digital.ai.
        - title: Value Stream Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Visualize, manage and optimize the flow of work through the DevOps lifecycle value stream.Users have different abilities depending on the role they have in a particular group or project. If a user is both in a project’s group and the project itself, the highest role is used.
              sections:
                - title: Details
                  content: |
                    * Use value stream analytics to identify:
                      * The amount of time it takes to go from an idea to production.
                      * The velocity of a given project.
                      * Bottlenecks in the development process.
                      * Factors that cause your software development lifecycle to slow down.
                    * View time spent in each development stage
                    * View the lead time and cycle time for issues
                    * View lead time for changes for merge requests
                    * View number of successful deployments
                    * Value stream analytics uses start and end events to measure the time that an issue or merge request spends in each stage.
                    * Custom DORA reporting for tracking metrics improvements, understand patterns in metrics trends, and compare performance between groups and projects
                - title: Improving our product capabilities
                  content: |
                    * Near-Term Goals
                      * Enhanced and improved Value Stream Analytics (VSA) usability for deeper actionable insights into the entire work process performance.
                      * Continue progress on custom reports and dashboards.
                      * Extend VSA integrations.
                      * Extend DORA integrations.
                    * Mid-Term Goals
                      * Consolidations and Centralization GitLab's analytics pages.
                      * Continue progress on VSA and DORA integrations.
                      * Add Biz value metrics into VSA (e.g. Adoption, OKRs, revenue, costs, CSAT)
                      * Develop and publish best practices for VSM and DORA.
                    * Top Vision Item(s)
                      * Value-stream Autonomous mapping tool
                      * Software delivery optimization AI recommendation
                      * Provide a DIY data analysis tool
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/analytics/value_stream_analytics
            - title: Digital.ai
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Unify, secure, and generate predictive insights across the software lifecycle
              sections:
                - title: Details
                  content: |
                    * Digital.ai offers a “Unified DevOps Platform” that incorporates all of their product suites. It promises to Integrate DevOps & Security capabilities to enable continuous delivery of software.
                    * The Platform offers an Artificial Intelligence component that promises to generate predictive insights to make smarter investments
                    * The Platform also indicates you can connect to existing processes, applications, and infrastructure to utilize data from existing tools outside of Digital.ai
              button:
                title: Documentation
                link: https://digital.ai/products/platform/
  competitor_cards:
    title: "More comparisons"
    cards:
      - name: "Harness"
        icon: agile-alt
        stage: Release
        description: How does GitLab compare to Harness in the Release stage?
        link: /competition/harness/
      - name: "Snyk"
        icon: secure-alt-2
        stage: Secure
        description: How does GitLab compare to Snyk in the Secure stage?
        link: /competition/snyk/
      - name: "Atlassian"
        icon: plan
        stage: Plan
        description: How does GitLab compare to Atlassian in the Plan stage?
        link: /competition/atlassian/
