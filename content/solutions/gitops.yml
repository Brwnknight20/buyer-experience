---
  title: GitLab for GitOps
  description: Foster collaboration between your infrastructure, operations, and development teams. Deploy more frequently with greater confidence while also increasing the stability, reliability, and security of your software environments.
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for GitOps
        subtitle: Infrastructure automation and collaboration for cloud native, multicloud, and legacy environments
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitlab for gitops"
    - name: 'copy-media'
      data:
        block:
          - header: Why GitLab for GitOps?
            text: |
              Foster collaboration between your infrastructure, operations, and development teams. Deploy more frequently with greater confidence while also increasing the stability, reliability, and security of your software environments. GitLab gives you version control, code review, and CI/CD in a single application for a seemless experience. Tight integrations with HashiCorp Terraform and Vault along with multi-cloud capabilities provide you with the best platform for infrastructure automation.
            link_href: /demo/
            link_text: Learn more
            video:
              video_url: https://www.youtube.com/embed/onFpj_wvbLM?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: 'copy-media'
      data:
        block:
          - header: What is GitOps?
            miscellaneous: |
              **GitOps** is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.
            media_link_href: /topics/gitops/
            media_link_text: Learn more
          - header: GitLab advantages
            miscellaneous: |
              * **A single app for SCM and CI/CD** Source Code Management and CI/CD are at the core of GitOps. A single app with a seamless experience keeps you productive.
              * **Tightly integrated with Terraform** Terraform has emerged as the industry standard for environment provisioning. GitLab partners with HashiCorp to make sure your tooling works the best together.
              * **Trusted by the world's largest engineering teams** From Goldman Sachs and Verizon to Ticketmaster and Siemens, more large enterprise trust GitLab with their code than anyone else.
          - header: How GitLab Enables GitOps
            miscellaneous: |
              * **Environment as code** stored in GitLab version control as a single source of truth.
              * **Teams collaborate** using GitLab's agile planning and code review.
              * **The same tool** used to plan, version, and deploy your application code works for your operations code as well.
              * **CI/CD for infrastructure automation** reconciles your environments with your SSoT in version control.
          - header: Capabilities
            miscellaneous: |
              * **Git-based version control** Use the Git tooling you already have as an interface for operations. Version your Infrastructure as Code along with configure, policy to make reproducible environments. During incidents roll back to a last known working state to lower your Mean Time to Resolution (MTTR).
              * **Code Review** Improve code quality, distribute best practices, and catch error before they go live with Merge Requests that keep track of and resolve threads, apply in-line suggestions and work asynchronously with in-line and general threaded comments.
              * **Protected Branches** Allow everyone to contribute with shared code repositories while limiting who can deploy to production with unique permissions for protected branches.
              * **CI/CD** Continually rated as a leader in CI/CD, GitLab provides powerful and scalable CI/CD built from the ground up into the same application as your agile planning and source code management for a seamless experience.
              * **Terraform integration** GitLab stores your Terraform state file and shows Terraform plan output directly in the merge request.
              * **Deploy anywhere** From containers and VMs to bare metal GitLab's deploys everywhere. Go multicloud with AWS, Azure, and Google Cloud and more.
    - name: copy-resources
      data:
        block:
          - resources:
              video:
                header: Videos
                links:
                  - text: GitOps Video Playlist
                    link: https://www.youtube.com/watch?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo&v=JtZfnrwOOAw
              report:
                header: Reports
                links:
                  - text: What is GitOps?
                    link: /topics/gitops/
                    data_ga_name: what is GitOps?
                    data_ga_location: body
              blog:
                header: Blogs
                links:
                  - text: Why collaboration technology is critical for GitOps
                    link: /blog/2019/11/04/gitlab-for-gitops-prt-1/
                    data_ga_name: Collaboration technology GitOps
                    data_ga_location: body
                  - text: How to use GitLab and Ansible to create infrastructure as code
                    link: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
                    data_ga_name: GitLab and Ansible
                    data_ga_location: body
              article:
                header: Articles
                links:
                  - text: '[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation'
                    link: /why/gitops-infrastructure-automation/
                    data_ga_name: 'GitOps: The future of Infrastructure Automation'
                    data_ga_location: body
